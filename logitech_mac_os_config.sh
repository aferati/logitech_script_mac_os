#!/bin/bash


# Starting the script


echo "Before starting, please enter the computer name here (4422...NB00X) : "
read computer_name
hostname $computer_name

cd ~/Downloads

# Installing Google Chrome

clear

echo "################################################"
echo "################################################"
echo "########### Installing Google Chrome ###########"
echo "################################################"
echo "################################################"

curl -# -Lo chrome.dmg https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg
hdiutil attach chrome.dmg
ditto -rsrc /Volumes/Google\ Chrome/Google\ Chrome.app /Applications/Google\ Chrome.app
hdiutil detach /Volumes/Google\ Chrome
rm chrome.dmg

# Installing Google Drive

clear

echo "################################################"
echo "################################################"
echo "########### Installing Google Drive ############"
echo "################################################"
echo "################################################"

curl -# -Lo googledrive.dmg "https://dl.google.com/drive-file-stream/GoogleDrive.dmg"
hdiutil attach googledrive.dmg
installer -pkg /Volumes/Install\ Google\ Drive/GoogleDrive.pkg -target /Applications
hdiutil detach /Volumes/Install\ Google\ Drive/
rm googledrive.dmg

# Installing Zoom

clear

echo "################################################"
echo "################################################"
echo "############### Installing Zoom ################"
echo "################################################"
echo "################################################"

curl -# -Lo zoom.pkg "https://logitech.zoom.us/client/latest/Zoom.pkg"
installer -pkg zoom.pkg -target /Applications
rm zoom.pkg

# Installing TeamviewerQS

clear

echo "################################################"
echo "################################################"
echo "############ Installing Teamviewer #############"
echo "################################################"
echo "################################################"

curl -# -Lo teamviewerqs.zip "https://customdesign.teamviewer.com/download/version_15x/v7kb7pb_mac/TeamViewerQS.zip"
unzip teamviewerqs.zip
sudo mv TeamViewerQS.app /Applications
rm -r __MACOSX
rm teamviewerqs.zip

# Installing Global Protect VPN

clear

echo "################################################"
echo "################################################"
echo "############ Installing Global VPN #############"
echo "################################################"
echo "################################################"

curl -# -Lo globalvpn.pkg "https://global.logitech.com/global-protect/getmsi.esp?version=none&platform=mac"
installer -pkg globalvpn.pkg -target /Applications
rm globalvpn.pkg

# Setting up ServiceDesk user

clear

echo "The apps are installed (don't forget to install filewave :D)."

echo "Press any key to exit."

read

exit 0;